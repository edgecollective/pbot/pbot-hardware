EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x14_Female J1
U 1 1 601038CF
P 1750 2550
F 0 "J1" H 1778 2526 50  0000 L CNN
F 1 "Conn_01x14_Female" V 1900 2350 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x14_P2.54mm_Vertical" H 1750 2550 50  0001 C CNN
F 3 "~" H 1750 2550 50  0001 C CNN
	1    1750 2550
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x14_Female J4
U 1 1 6010460F
P 2800 2550
F 0 "J4" H 2828 2526 50  0000 L CNN
F 1 "Conn_01x14_Female" V 2950 2350 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x14_P2.54mm_Vertical" H 2800 2550 50  0001 C CNN
F 3 "~" H 2800 2550 50  0001 C CNN
	1    2800 2550
	1    0    0    -1  
$EndComp
Text GLabel 1550 3250 0    50   Input ~ 0
BAT
Text GLabel 1550 3050 0    50   Input ~ 0
USB
Text GLabel 1550 2950 0    50   Input ~ 0
13
Text GLabel 1550 2850 0    50   Input ~ 0
12
Text GLabel 1550 2750 0    50   Input ~ 0
11
Text GLabel 1550 2650 0    50   Input ~ 0
10
Text GLabel 1550 2550 0    50   Input ~ 0
9
Text GLabel 1550 2450 0    50   Input ~ 0
7
Text GLabel 1550 2350 0    50   Input ~ 0
5
Text GLabel 1550 2250 0    50   Input ~ 0
SCL
Text GLabel 1550 2150 0    50   Input ~ 0
SDA
Text GLabel 1550 2050 0    50   Input ~ 0
TX
Text GLabel 1550 1950 0    50   Input ~ 0
RX
Text GLabel 2600 3250 0    50   Input ~ 0
RST
Text GLabel 2600 3050 0    50   Input ~ 0
AREF
Text GLabel 2600 2950 0    50   Input ~ 0
VH
Text GLabel 2600 2850 0    50   Input ~ 0
A0
Text GLabel 2600 2750 0    50   Input ~ 0
A1
Text GLabel 2600 2650 0    50   Input ~ 0
A2
Text GLabel 2600 2550 0    50   Input ~ 0
A3
Text GLabel 2600 2450 0    50   Input ~ 0
A4
Text GLabel 2600 2350 0    50   Input ~ 0
A5
Text GLabel 2600 2250 0    50   Input ~ 0
SCK
Text GLabel 2600 2150 0    50   Input ~ 0
MOSI
Text GLabel 2600 2050 0    50   Input ~ 0
MISO
Text GLabel 2600 1950 0    50   Input ~ 0
2
Text GLabel 2300 1400 1    50   Input ~ 0
EN
Text GLabel 2000 1400 1    50   Input ~ 0
3
Text GLabel 1900 1400 1    50   Input ~ 0
4
$Comp
L Connector:Conn_01x05_Female J3
U 1 1 6010DEDD
P 2100 1600
F 0 "J3" H 2128 1626 50  0000 L CNN
F 1 "Conn_01x05_Female" V 2250 1250 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x05_P2.54mm_Vertical" H 2100 1600 50  0001 C CNN
F 3 "~" H 2100 1600 50  0001 C CNN
	1    2100 1600
	0    1    1    0   
$EndComp
Text GLabel 2100 1400 1    50   Input ~ 0
swclk
Text GLabel 2200 1400 1    50   Input ~ 0
swdio
Text Notes 1750 850  0    118  ~ 0
ItsyBitsy
Text Notes 3650 850  0    118  ~ 0
RFM95 Breakout
$Comp
L Connector:Conn_01x09_Female J5
U 1 1 6013330B
P 4300 1650
F 0 "J5" H 4328 1676 50  0000 L CNN
F 1 "Conn_01x09_Female" H 4328 1585 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x09_P2.54mm_Vertical" H 4300 1650 50  0001 C CNN
F 3 "~" H 4300 1650 50  0001 C CNN
	1    4300 1650
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 60137BFA
P 1150 3150
F 0 "#PWR0101" H 1150 2900 50  0001 C CNN
F 1 "GND" H 1155 2977 50  0000 C CNN
F 2 "" H 1150 3150 50  0001 C CNN
F 3 "" H 1150 3150 50  0001 C CNN
	1    1150 3150
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0102
U 1 1 6013BC2E
P 2200 3150
F 0 "#PWR0102" H 2200 3000 50  0001 C CNN
F 1 "+3V3" H 2200 3300 50  0000 C CNN
F 2 "" H 2200 3150 50  0001 C CNN
F 3 "" H 2200 3150 50  0001 C CNN
	1    2200 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 3150 2600 3150
Wire Wire Line
	1150 3150 1550 3150
$Comp
L power:+3V3 #PWR0103
U 1 1 60150602
P 3900 1100
F 0 "#PWR0103" H 3900 950 50  0001 C CNN
F 1 "+3V3" H 3900 1250 50  0000 C CNN
F 2 "" H 3900 1100 50  0001 C CNN
F 3 "" H 3900 1100 50  0001 C CNN
	1    3900 1100
	0    1    1    0   
$EndComp
Wire Wire Line
	3900 1450 3900 1100
Wire Wire Line
	4000 1450 4000 1300
Wire Wire Line
	4000 1300 3700 1300
$Comp
L power:GND #PWR0104
U 1 1 60152A90
P 3700 1300
F 0 "#PWR0104" H 3700 1050 50  0001 C CNN
F 1 "GND" H 3705 1127 50  0000 C CNN
F 2 "" H 3700 1300 50  0001 C CNN
F 3 "" H 3700 1300 50  0001 C CNN
	1    3700 1300
	0    1    1    0   
$EndComp
Text GLabel 4100 1450 1    50   Input ~ 0
EN
Text GLabel 4300 1450 1    50   Input ~ 0
SCK
Text GLabel 4400 1450 1    50   Input ~ 0
MISO
Text GLabel 4500 1450 1    50   Input ~ 0
MOSI
$Comp
L Connector:Screw_Terminal_01x02 J2
U 1 1 601586E3
P 2050 4400
F 0 "J2" H 2130 4392 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 2130 4301 50  0000 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_PT-1,5-2-5.0-H_1x02_P5.00mm_Horizontal" H 2050 4400 50  0001 C CNN
F 3 "~" H 2050 4400 50  0001 C CNN
	1    2050 4400
	1    0    0    -1  
$EndComp
Text Notes 1550 4050 0    118  ~ 0
Battery Input
Text GLabel 1850 4400 0    50   Input ~ 0
BAT
$Comp
L power:GND #PWR0105
U 1 1 6015E43A
P 1850 4500
F 0 "#PWR0105" H 1850 4250 50  0001 C CNN
F 1 "GND" H 1855 4327 50  0000 C CNN
F 2 "" H 1850 4500 50  0001 C CNN
F 3 "" H 1850 4500 50  0001 C CNN
	1    1850 4500
	1    0    0    -1  
$EndComp
Text GLabel 4200 1450 1    50   Input ~ 0
2
Text GLabel 4600 1450 1    50   Input ~ 0
A5
Text GLabel 4700 1450 1    50   Input ~ 0
A4
$EndSCHEMATC
